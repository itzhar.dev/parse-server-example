const baseUrl = "http://localhost:1337";
var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var ParseDashboard = require('parse-dashboard');

var S3Adapter = require('@parse/s3-files-adapter');
var s3Adapter = new S3Adapter('AKIA5MVKSSQOJVGNFBFG',
    'C3B/QAQ+rnW4OxAiKGUJUC+lmsOgqLj44Cm6tP5P', 'ssa.parse.server.example', {
        region: 'us-east-1',
        bucketPrefix: '',
        baseUrl: 'https://s3.amazonaws.com/ssa.parse.server.example',
        baseUrlDirect: true,
        signatureVersion: 'v4',
        validateFilename: (filename) => {
            if (filename.length > 1024) {
                return 'Filename too long.';
            }
            return null; // Return null on success
        }
    });

const PushAdapter = require('@parse/push-adapter').default;
const pushOptions = {
    ios: { /* iOS push options */ } ,
    android: { /* android push options */ }
}

var api = new ParseServer({
    databaseURI: 'mongodb://parse:12345a@ds211096.mlab.com:11096/parse-server-example', // Connection string for your MongoDB database
    cloud: './cloud/main.js', // Absolute path to your Cloud Code
    appId: 'myAppId',
    masterKey: 'myMasterKey', // Keep this key secret!
    fileKey: 'optionalFileKey',
    serverURL: baseUrl + '/parse', // Don't forget to change to https if needed
    liveQuery: {
        classNames: ['Post', 'AnotherClass']
    },
    filesAdapter: s3Adapter
});

var options = { allowInsecureHTTP: false };

var dashboard = new ParseDashboard({
    "apps": [
        {
            "serverURL": baseUrl + "/parse",
            "appId": "myAppId",
            "masterKey": "myMasterKey",
            "appName": "ParseServer Example"
        }
    ]
}, options);

var app = express();

// make the Parse Server available at /parse
app.use('/parse', api);

// make the Parse Dashboard available at /dashboard
app.use('/dashboard', dashboard);

let httpServer = require('http').createServer(app);
httpServer.listen(1337);
var parseLiveQueryServer = ParseServer.createLiveQueryServer(httpServer);
